---
layout: handbook-page-toc
title: "IAM.1.08 - New Access Provisioning"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# IAM.1.08 - New Access Provisioning

## Control Statement
New user access requests follow an access provisioning workflow. Request forms and authorization are documented and retained.


## Context
The purpose of this control is to ensure there is a process in place to review and authorize new user account requests. Ensuring only people who require access to a system or service receive access, helps improve GitLab's overall security posture by limiting the number of accounts with access and reducing the overall likelihood of an account being compromised.

## Scope
This control applies to any system or service where user accounts can be provisioned.

## Ownership
Control ownership:
 * IT Operations

Process ownership:
 * IT Operations
 * Managers
 * Systems Owners

## Guidance
* Access requested should be aligned with the user's expected job responsibilities
* The access requested should be approved by appropriate management/system owner/data owner prior to being granted
* Access can be granted on or after employee hire date
* Individuals who provisioned access have to be separate from the individuals that approved access
* The access granted should align with access requested


## Additional control information and project tracking
Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [New Access Provisioning control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/-/issues/1700).

## Policy Reference

- [Access Control Policy](/handbook/engineering/security/#access-control-policy-and-procedures)
- [Access Request Process](https://about.gitlab.com/handbook/engineering/#access-requests)
- [Access Management Process](https://about.gitlab.com/handbook/engineering/security/#access-management-process)
- [Access Request Template](https://gitlab.com/gitlab-com/team-member-epics/access-requests/issues/new?issuable_template=New%20Access%20Request)

## Framework Mapping
*  SOC2 CC
     * CC6.2 - Prior to issuing system credentials and granting system access, the entity registers and authorizes new internal and external users whose access is administered by the entity. For those users whose access is administered by the entity, user system credentials are removed when user access is no longer authorized.

*  PCI
    * 7.1.4 - Limit access to system components and cardholder data to only those individuals whose job requires such access.
    * 8.1.2 - Control addition, deletion, and modification of user IDs, credentials, and other identifier objects.
